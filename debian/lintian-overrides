# Lintian overrides for request-tracker4

# Allowing world read access on these directories allows bypassing of
# application defined permissions.

non-standard-dir-perm 2755 != 0755 [var/log/request-tracker4/]
non-standard-dir-perm 2750 != 0755 [var/cache/request-tracker4/]
non-standard-dir-perm 2750 != 0755 [var/cache/request-tracker4/mason_data/]
non-standard-dir-perm 2750 != 0755 [var/cache/request-tracker4/mason_data/etc/]
non-standard-dir-perm 2750 != 0755 [var/cache/request-tracker4/mason_data/obj/]
non-standard-dir-perm 2750 != 0755 [var/cache/request-tracker4/mason_data/cache/]
non-standard-dir-perm 2750 != 0755 [var/cache/request-tracker4/session_data/]
non-standard-dir-perm 2750 != 0755 [var/lib/request-tracker4/]
non-standard-dir-perm 2750 != 0755 [var/lib/request-tracker4/data/]
non-standard-dir-perm 0700 != 0755 [var/lib/request-tracker4/data/gpg/]

# This file is part of a customised version of ckeditor
embedded-javascript-library please use ckeditor [usr/share/request-tracker4/static/RichText/ckeditor.js]
embedded-javascript-library please use libjs-jquery [usr/share/request-tracker4/static/RichText/adapters/jquery.js]

# jquery 1.9 not available in Debian.
embedded-javascript-library please use libjs-jquery-tablesorter [usr/share/request-tracker4/static/js/jquery.tablesorter.min.js]
embedded-javascript-library please use libjs-jquery-cookie [usr/share/request-tracker4/static/js/jquery.cookie.js]

# jquery 1.12.4 not available in Debian.
embedded-javascript-library please use libjs-jquery-ui [usr/share/request-tracker4/static/css/base/jquery-ui.css]
embedded-javascript-library please use libjs-jquery-ui [usr/share/request-tracker4/static/js/jquery-ui.min.js]

# Encoded file
national-encoding [usr/share/request-tracker4/static/js/jquery.event.hover-1.0.js]

# We give full paths in the generated cron file.
command-with-path-in-maintainer-script /usr/sbin/rt-email-dashboards-4 (in test syntax) [postinst:138]
command-with-path-in-maintainer-script /usr/sbin/rt-email-dashboards-4 (plain script) [postinst:138]
command-with-path-in-maintainer-script /usr/sbin/rt-email-digest-4 (in test syntax) [postinst:136]
command-with-path-in-maintainer-script /usr/sbin/rt-email-digest-4 (in test syntax) [postinst:137]
command-with-path-in-maintainer-script /usr/sbin/rt-email-digest-4 (plain script) [postinst:136]
command-with-path-in-maintainer-script /usr/sbin/rt-email-digest-4 (plain script) [postinst:137]

# These aren't documentation files.
package-contains-documentation-outside-usr-share-doc [usr/share/request-tracker4/html/*
package-contains-documentation-outside-usr-share-doc [usr/share/request-tracker4/static/RichText/*

# Mason has multiple directories, including cache.
repeated-path-segment cache [var/cache/request-tracker4/mason_data/cache/]

# The debconf templates are accessed in a for loop which is why lintian can't
# find them.
unused-debconf-template request-tracker4/commentaddress*
unused-debconf-template request-tracker4/correspondaddress*
unused-debconf-template request-tracker4/organization*
unused-debconf-template request-tracker4/rtname*
unused-debconf-template request-tracker4/webbaseurl*
unused-debconf-template request-tracker4/webpath*

# Documentation doesn't make sense for a dummy service.
systemd-service-file-missing-documentation-key [lib/systemd/system/request-tracker4.service]

# Status doesn't make sense for a dummy init script
init.d-script-does-not-implement-status-option [etc/init.d/request-tracker4]

# These packages don't provide a version of LGPL.
copyright-refers-to-symlink-license usr/share/common-licenses/LGPL
